\documentclass{beamer}

\usepackage{fontspec}
\usepackage{float}
\usepackage{subfig}
\usepackage{graphicx}
\usepackage{pgfplots}
\usepackage{listings}
\pgfplotsset{compat=1.17}

% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{6} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{6}  % for normal

% Custom colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0.5,0.5,1}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\definecolor{comment}{rgb}{0,0.5,0.5}

%Python synthax highligh
\lstset{
language=Python,
basicstyle=\ttm,
numbers=left,
numberstyle=\tiny,
otherkeywords={self, as, True, False},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
commentstyle=\color{comment},
frame=tb,                         % Any extra options here
showstringspaces=false            %
}

\title{ Wavelets Analysis }
\subtitle{ An Introduction }
\author[ Moreno ]{ Moreno Sarria }

\usetheme{Berlin}
\usecolortheme{beaver}

\AtBeginSection[]
{
  \begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents[currentsection]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents[currentsubsection]
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents
\end{frame}

\section{ Math Definitions }

\begin{frame}
  \frametitle{ Integral Transforms }

  A integral transform is a mathematical procedure that is applied to a
  function to get another function.

  We have seen some examples such as:

  \begin{enumerate}
    \item<2-> Fourier transform.
    \begin{align*}
      (Ff)(\omega) = \int_{-\infty}^{\infty} f(t) e^{-2\pi i t \omega} dt
    \end{align*}

    \item<3-> Hilbert transform.
    \begin{align*}
      (Hf)(t) = \frac{1}{\pi} \text{P.V.} \int_{-\infty}^{\infty}
      \frac{f(\tau)}{t-\tau} d\tau
    \end{align*}

    \item<4-> Wavelets transform.
    \begin{align*}
      (W_{\psi}f)(a, b) = \frac{1}{\sqrt{a}} \int_{-\infty}^{\infty}
      f(t) \bar{\psi}\left( \frac{t-b}{a} \right) dt
    \end{align*}

  \end{enumerate}

\end{frame}

\begin{frame}
  \frametitle{ The Wavelet Transform }

  \begin{block}{Wavelet Transform}

    Having a wavelet mother \(\psi(t)\), that satisfies some conditions, it is
    possible to define the continuous wavelet transform,
    \begin{align*}
      (W_{\psi}f)(a, b) = \frac{1}{\sqrt{a}} \int_{-\infty}^{\infty}
      f(t) \bar{\psi}\left( \frac{t - b}{a} \right) dt,
    \end{align*}
    as well as the discrete wavelet transform,
    \begin{align*}
      (W_{\psi}f)_{k,m} = \frac{1}{\sqrt{2^k}} \int_{-\infty}^{\infty}
      f(t) \bar{\psi}\left( \frac{t - 2^k m}{2^k} \right) dt.
    \end{align*}

  \end{block}

\end{frame}

\begin{frame}
  \frametitle{ What it means? }

  Well, we have that the discrete wavelet transform is,
  \begin{align*}
    (W_{\psi}f)_{k,m} = \frac{1}{\sqrt{2^k}} \int_{-\infty}^{\infty}
    f(t) \bar{\psi}\left( \frac{t - 2^k m}{2^k} \right) dt.
  \end{align*}
  But, what it means? And most important, How it works in real data?

\end{frame}

\section{ Basics }

\begin{frame}
  \frametitle{ What does the wavelet transform do? }

  Imagine we have a sampled time series,
  \begin{align*}
    x[n] = \{x_1, x_2, x_3, \cdots, x_N\}.
  \end{align*}
  The wavelet transform will split into two new arrays,
  \begin{align*}
    a[n] &= \{t_1, t_2, t_3, \cdots, t_{N/2}\},\\
    d[n] &= \{f_1, f_2, f_3, \cdots, f_{N/2}\},
  \end{align*}
  where no information is lost.

\end{frame}

\begin{frame}
  \frametitle{ How does the wavelet transform work? }

  Having sampled time series,
  \begin{align*}
    x[n] = \{x_1, x_2, x_3, \cdots, x_N\},
  \end{align*}
  and a wavelet as well as its scaling signal,
  \begin{align*}
    W_{1, m}[n] &= \{W_1, W_2, W_3, \cdots, W_N\}_{1, m}\\
    S_{1, m}[n] &= \{S_1, S_2, S_3, \cdots, S_N\}_{1, m}
  \end{align*}
  the wavelets transform if given with,
  \begin{align*}
    a[n] &= \sum_{i=1}^{N} x[i] W_{1, n}[i],\\
    d[n] &= \sum_{i=1}^{N} x[i] S_{1, n}[i].
  \end{align*}

\end{frame}

\begin{frame}
  \frametitle{ How does the wavelet transform work? }

  Where,
  \begin{align*}
    a[n] &= x \cdot W_{1, n} = \sum_{i=1}^{N} x[i] W_{1, n}[i].\\
  \end{align*}
  \begin{figure}[H]
    \centering
    \begin{tikzpicture}[scale = 0.5]

      \node at(-9, 2) (x_1) {\( x[1] \)};
      \node at(-3, 2) (x_2) {\( x[2] \)};
      \node at(3, 2) (x_3) {\( \cdots \)};
      \node at(9, 2) (x_4) {\( x[N] \)};

      \node at(-9, 0) (W_1) {\( W_{1, n}[1] \)};
      \node at(-3, 0) (W_2) {\( W_{1, n}[2] \)};
      \node at(3, 0) (W_3) {\( \cdots \)};
      \node at(9, 0) (W_4) {\( W_{1, n}[N] \)};

      \node at(-9, -2) (xW_1) {\( x[1] \cdot W_{1, n}[1] \)};
      \node at(-6, -2) (xW_2) {\( + \)};
      \node at(-3, -2) (xW_3) {\( x[2] \cdot W_{1, n}[2] \)};
      \node at(0, -2) (xW_4) {\( + \)};
      \node at(3, -2) (xW_5) {\( \cdots \)};
      \node at(5, -2) (xW_6) {\( + \)};
      \node at(9, -2) (xW_7) {\( x[N] \cdot W_{1, n}[N] \)};

      \draw [<-] (W_1) -- (x_1) node[midway, left] {\(\cdot\)};
      \draw [<-] (W_2) -- (x_2) node[midway, left] {\(\cdot\)};
      \draw [<-] (W_4) -- (x_4) node[midway, left] {\(\cdot\)};

      \draw [<-] (xW_1) -- (W_1) node[midway, left] {\(=\)};
      \draw [<-] (xW_3) -- (W_2) node[midway, left] {\(=\)};
      \draw [<-] (xW_7) -- (W_4) node[midway, left] {\(=\)};

    \end{tikzpicture}
    \caption{Sum illustration}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{ How does the wavelet transform work? }

  \begin{figure}[H]
    \centering
    \begin{tikzpicture}[
      scale = 0.5,
      squarednode/.style={rectangle, draw=red!60, fill=red!5, very thick, minimum size=5mm},
      roundnode/.style={rectangle, draw=green!60, fill=green!5, very thick, minimum size=5mm}
      ]

      \node [squarednode] at(-4, 0) (x_1) {\( x[n] \)};
      \node [squarednode] at(4, -3) (x_2) {\( d[n] \)};
      \node [squarednode] at(4, 3) (x_3) {\( a[n] \)};

      \node [roundnode] at(0, -3) (W_1) {\( W_{1, m}[n] \)};
      \node [roundnode] at(0, 3) (W_2) {\( S_{1, m}[n] \)};

      \draw (x_1) -- (0, 0);
      \draw (0, 0) -- (W_1);
      \draw (0, 0) -- (W_2);

      \draw [<-] (x_2) -- (W_1);
      \draw [<-] (x_3) -- (W_2);

    \end{tikzpicture}
    \caption{Wavelet transform}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{ Higher level wavelet transform }

  \begin{figure}[H]
    \centering
    \begin{tikzpicture}[
      scale = 0.4,
      squarednode/.style={rectangle, draw=red!60, fill=red!5, very thick, minimum size=1mm},
      roundnode/.style={rectangle, draw=green!60, fill=green!5, very thick, minimum size=1mm}
      ]

      \node [squarednode] at(-4, 0) (x_1) {\( x[n] \)};
      \node [squarednode] at(4, -3) (x_2) {\( d_1[n] \)};
      \node [squarednode] at(4, 3) (x_3) {\( a_1[n] \)};

      \node [squarednode] at(12, 0) (x_4) {\( d_2[n] \)};
      \node [squarednode] at(12, 6) (x_5) {\( a_2[n] \)};

      \node [squarednode] at(20, 3) (x_6) {\( d_3[n] \)};
      \node [squarednode] at(20, 9) (x_7) {\( a_3[n] \)};


      \node [roundnode] at(0, -3) (W_1) {\( W_{1, m}[n] \)};
      \node [roundnode] at(0, 3) (W_2) {\( S_{1, m}[n] \)};

      \node [roundnode] at(8, 0) (W_3) {\( W_{1, m}[n] \)};
      \node [roundnode] at(8, 6) (W_4) {\( S_{1, m}[n] \)};

      \node [roundnode] at(16, 3) (W_5) {\( W_{1, m}[n] \)};
      \node [roundnode] at(16, 9) (W_6) {\( S_{1, m}[n] \)};

      \node at(22, 6) (W_7) {\( \cdots \)};


      \draw (x_1) -- (0, 0);
      \draw (0, 0) -- (W_1);
      \draw (0, 0) -- (W_2);

      \draw [<-] (x_2) -- (W_1);
      \draw [<-] (x_3) -- (W_2);

      \draw (x_3) -- (8, 3);
      \draw (8, 3) -- (W_3);
      \draw (8, 3) -- (W_4);

      \draw [<-] (x_4) -- (W_3);
      \draw [<-] (x_5) -- (W_4);

      \draw (x_5) -- (16, 6);
      \draw (16, 6) -- (W_5);
      \draw (16, 6) -- (W_6);

      \draw [<-] (x_6) -- (W_5);
      \draw [<-] (x_7) -- (W_6);

    \end{tikzpicture}
    \caption{Higher level wavelet transform}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{ Mother wavelet }

  Even though higher level wavelet transform can be reached applying multiple
  times the 1-level transform, it is more efficient to use a direct method.

  This method consist in having a mother wavelet \( \phi(t) \), and then,
  produce different levels of wavelet transform as the mother wavelet is
  scaled.,
  \begin{align*}
    W_{\psi}(t)_{k, m} = \frac{1}{\sqrt{2^k}} \psi \left( \frac{t - 2^k m}{2^k}
    \right),
  \end{align*}
  where \(k\) is the wavelet transform level.

\end{frame}

\begin{frame}
  \frametitle{ The Wavelet Transform }

  \begin{block}{Wavelet Transform}

    Having a wavelet mother \(\psi(t)\), that satisfies some conditions, it is
    possible to define the discrete wavelet transform,
    \begin{align*}
      (W_{\psi}f)_{k,m} = \frac{1}{\sqrt{2^k}} \int_{-\infty}^{\infty}
      f(\tau) \bar{\psi}\left( \frac{\tau - 2^k m}{2^k} \right) d\tau.
    \end{align*}
    as well as the continuous wavelet transform,
    \begin{align*}
      (W_{\psi}f)(a, t) = \frac{1}{\sqrt{a}} \int_{-\infty}^{\infty}
      f(\tau) \bar{\psi}\left( \frac{\tau - t}{a} \right) d\tau,
    \end{align*}

  \end{block}

\end{frame}

\section{Examples}

\begin{frame}[t]{The Haar Wavelet}

  An example of a wavelet is the Haar wavelet, which is a square-shape
  function.

  The nature of its mother wavelet and scaling function let's us interpret the
  wavelet transformation as a weighted average and difference.

  \begin{block}{Haar wavelets}
    The mother wavelet \(\psi(t)\) and the scaling function \(\phi(t)\),
    \begin{align*}
      \psi(t) &=
      \begin{cases}
        1 & 0 <= t < \frac{1}{2} \\
        -1 & \frac{1}{2} <= t < 1\\
        0 & \text{otherwise}\\
      \end{cases}&
      \phi(t) &=
      \begin{cases}
        1 & 0 <= t < 1 \\
        0 & \text{otherwise}\\
      \end{cases}.
    \end{align*}

  \end{block}

\end{frame}

\begin{frame}[t]{The Haas Wavelet}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/wavelet.pdf}
        \caption{The Haar mother wavelet.}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/scaling.pdf}
        \caption{The Haar scaling function.}
      \end{figure}

    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[t]{The Wavelet Transform of a signal}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/signal.pdf}
        \caption{Input signal.}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/haar.pdf}
        \caption{Wavelet transform of the signal with Haar wavelet.}
      \end{figure}

    \end{column}

  \end{columns}

\end{frame}

\section{Properties}

\begin{frame}[t]{Energy Conservation}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/energy.pdf}
        \caption{Cumulative energy of the signal and the transform.}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}

      The transform transferred most of the energy to the trend (\(a[n]\)) but
      keeps the total energy invariant.

      The cumulative energy is given by,
      \begin{align*}
        E_c[n] = \frac{\sum_{i=1}^{n} {x[i]}^2}{\sum_{i=1}^{N} {x[i]}^2}.
      \end{align*}

    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[t]{Frequency Properties}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/transfer.pdf}
        \caption{Wavelet transform of the signal with Haar wavelet.}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}

      The transform with the scaling function works as a low-pass filter, while
      the wavelet acts as a high-pass filter.

    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[t]{Uncertainty Principle}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      The wavelet transform is restricted by the uncertainty principle,
      \begin{align*}
        \Delta t\Delta \omega \geq \frac{1}{2},
      \end{align*}
      then, the less is the time uncertainty, the more is the frequency
      uncertainty and viceversa.

    \end{column}

    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/spectrogram.pdf}
        \caption{Wavelet transform of the signal with Haar wavelet.}
      \end{figure}

    \end{column}

  \end{columns}

\end{frame}

\section{Applications}

\begin{frame}[t]{Signal Compression}

  \begin{columns}

    \begin{column}{0.5\textwidth}

      It is possible to compress the signal using the wavelets analysis.

      The trend will have similar information of the original signal, with the
      half of the original points.

    \end{column}

    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/compress.pdf}
        \caption{Compressed signal with the Haar wavelet transform.}
      \end{figure}
    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[t]{Signal De-noising}

  \begin{columns}

    \begin{column}{0.4\textwidth}

      The fluctuation will keep the information of high-frequency noise of the
      signal, then, if we apply the inverse transform, reducing the weight of
      the fluctuations, we will reduce the noise.

    \end{column}

    \begin{column}{0.6\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/denoise_noise.pdf}
        \caption{Noise reduction with a 6-level Haar wavelet transform.}
      \end{figure}
    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[t]{Image manipulating}

  \begin{columns}

  \begin{column}{0.6\textwidth}

    \begin{figure}
      \centering
      \includegraphics[width=0.8\textwidth]{graph/lena.png}
      \caption{Edges identification.}
    \end{figure}
  \end{column}

    \begin{column}{0.4\textwidth}

      It is possible to apply a wavelet transform to an image since it can be
      interpreted as a matrix.

      With the difference result, it is possible to get a edge identification.

    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[t]{Spectrogram and Scalogram}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/scalogram.pdf}
        \caption{Scalogram of the signal.}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/spectrogram.pdf}
        \caption{Spectrogram of the signal.}
      \end{figure}

    \end{column}

  \end{columns}

\end{frame}

\section{Tools}
\subsection{For Python}

\begin{frame}[fragile]{Scipy}

  \begin{columns}
    \begin{column}{0.4\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{graph/scipy.jpg}
        \caption{Scalogram of the signal.}
      \end{figure}
    \end{column}

    \begin{column}{0.6\textwidth}

      \begin{lstlisting}[language=Python]
from scipy.signal import cwt, morlet

N = 1024

n = np.arange(0, N)
dn = 1.0
m = (n-1)/N
signal = 20 * m**2 * (m-1)**4 * np.cos(12*np.pi*m)

widths = np.arange(1, 12)
cwtmatr = cwt(signal, morlet, widths)
      \end{lstlisting}

    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[fragile]{PyWavelets}

  \begin{columns}
    \begin{column}{0.4\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{graph/python.png}
        \caption{Scalogram of the signal.}
      \end{figure}
    \end{column}

    \begin{column}{0.6\textwidth}

      \begin{lstlisting}[language=Python]
import pywt

N = 1024

n = np.arange(0, N)
dn = 1.0
m = (n-1)/N
signal = 20 * m**2 * (m-1)**4 * np.cos(12*np.pi*m)

widths = np.arange(1, 12)
cwtmatr, freqs = pywt.cwt(signal, widths, "morl")
      \end{lstlisting}

    \end{column}

  \end{columns}

\end{frame}

\subsection{For MatLab}

\begin{frame}[t]{wavemenu}

  \begin{columns}
    \begin{column}{0.5\textwidth}

      \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{graph/wavemenu.png}
        \caption{Scalogram of the signal.}
      \end{figure}
    \end{column}

    \begin{column}{0.5\textwidth}

      There is a comand in MatLab (\texttt{wavemenu}), that display a pop-up
      window with wavelets options.

    \end{column}

  \end{columns}

\end{frame}

\end{document}
