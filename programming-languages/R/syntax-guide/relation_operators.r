# Relation Operators.

# Library used for color printing.
library(crayon)

cat(cyan("Relation Operators\n\n"))

# Variables.

## Vectors.

vector.numerical.a = c(12.34, 12.34, 2.44, -0.2, 0.0, -12.34)
vector.numerical.b = c(11.2, 12.34, 0.0, 33.21, -0.1, -12.34)

vector.integer.a = c(1L, 2L, 3L, 4L, 5L)
vector.integer.b = c(12L, 2L, -23L, 4L, -11L)

vector.complex.a = c(1 + 1i, 1, 1i, 3 + 5i, 13.2 - 1i, -3 - 4i)
vector.complex.b = c(2 - 3i, 9 + 2i, 4 - 2i, 3 + 5i, 12 + 76.0i, -3 - 4i)

vector.character.a = c("AA", "BB", "hello", "bye", "yes", "no")
vector.character.b = c("CC", "BB", "hello.", "bye", "Yes", "no")

## Matrices.

matrix.numerical.a = matrix(
  c(
    13.64, 67.08, 81.58, -35.88, 11.82, -12.09, 83.63, -36.91,
    142.52, 101.34, 100.01, 102.08, 14.15, -31.06, 19.44, -25.77,
    -48.64, 5.26, -43.03, 56.89, 94.59, -33.83, 33.87, -4.07
  ),
  ncol=8, nrow=3
)
matrix.numerical.b = matrix(
  c(
    13.64, 19.36, 70.33, 65.96, 69.32, 76.62, 117.40, 60.06,
    -46.50, 101.34, 68.73, 142.57, 25.74, -25.02, -0.30, -11.80,
    35.26, 116.21, 24.08, 56.89, 94.59, -33.83, 90.71, 35.71
  ),
  ncol=8, nrow=3
)

matrix.integer.a = matrix(
  c(
    -33L, 36L, -33L, 74L, 129L, 1L, 109L, 118L,
    41L, -43L, 113L, -40L, 116L, 26L, 64L, -28L,
    117L, 122L, 38L, 127L, 92L, -11L, 32L, 124L
  ),
  ncol=8, nrow=3
)
matrix.integer.b = matrix(
  c(
    7L, 6L, -33L, 74L, 129L, 71L, 5L, 20L, 110L,
    66L, -46L, 37L, -40L, 105L, 89L, 64L, 43L,
    -16L, 6L, 38L, 127L, 92L, 147L, 33L
  ),
  ncol=8, nrow=3
)

matrix.complex.a = matrix(
  c(
    30.26 + 126.56i, 108.14 - 21.54i, -43.32 - 14.78i, 11.55 - 30.24i,
    1.36 - 28.65i, 79.16 + 69.67i, 70.85 - 12.63i, 21.35 + 11.54i,
    79.58 + 32.41i, 81.69 + 21.08i, -37.75 + 22.63i, 39.81 + 129.32i,
    7.92 + 102.12i, 129.56 - 14.23i, 70.57 - 47.64i, 107.12 - 28.91i,
    118.19 + 41.98i, -4.01 + 15.93i, 72.71 + 143.68i, 102.37 + 117.68i,
    22.04 +23.16i, -0.85 + 79.05i, -44.43 + 107.35i, 17.45 + 100.71i
  ),
  ncol=8, nrow=3
)
matrix.complex.b = matrix(
  c(
    -42.42 + 38.08i, 108.14 - 21.54i, 84.33 - 15.53i, 17.35 + 90.87i,
    16.24 + 113.30i, 144.14 + 14.11i, 68.94 + 15.54i, 18.64 - 23.92i,
    -8.88 - 21.25i, -44.75 + 77.42i, -37.75 + 22.63i, -8.48 + 8.65i,
    105.43 + 60.51i, -5.48 + 135.60i, 9.22 + 12.57i, -44.13 - 47.55i,
    -3.51 + 85.99i, -4.01 + 15.93i, 104.79 + 118.14i, 4.13 + 59.89i,
    -8.12 + 45.83i, -32.60 + 89.87i, -39.12 + 11.14i, -38.76 + 82.17i
  ),
  ncol=8, nrow=3
)

matrix.character.a = matrix(
  c(
    "ship", "bell", "haunt", "shoe", "potato", "pocket", "hate", "wail",
    "stormy", "sneeze", "scared", "raspy", "unaccountable", "sharp", "shiver",
    "error", "greet", "wholesale", "delicious", "crush", "believe",
    "abhorrent", "drag", "blow"
  ),
  ncol=8, nrow=3
)
matrix.character.b = matrix(
  c(
    "cellar", "chief", "acceptable", "shoe", "potato", "pocket", "muddled",
    "shiver", "outrageous", "children", "tug", "ready", "street", "delay",
    "replace", "bury", "quartz", "satisfy", "seat", "painful", "steadfast",
    "porter", "belief", "blow"
  ),
  ncol=8, nrow=3
)

## Vectors.
cat(green("Vector:\n"))

cat(yellow("\nGreater:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na > b: ", vector.numerical.a > vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na > b: ", vector.integer.a > vector.integer.b, "\n\n")

cat(yellow("\nLess:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na < b: ", vector.numerical.a < vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na < b: ", vector.integer.a < vector.integer.b, "\n\n")

cat(yellow("\nEqual:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na == b: ", vector.numerical.a == vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na == b: ", vector.integer.a == vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na == b: ", vector.complex.a == vector.complex.b, "\n\n")
cat("a: ", vector.character.a, "\tb: ", vector.character.b,
"\na == b: ", vector.character.a == vector.character.b, "\n\n")

cat(yellow("\nGreater or Equal:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na >= b: ", vector.numerical.a >= vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na >= b: ", vector.integer.a >= vector.integer.b, "\n\n")

cat(yellow("\nLess or Equal:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na <= b: ", vector.numerical.a <= vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na <= b: ", vector.integer.a <= vector.integer.b, "\n\n")

cat(yellow("\nDifferent:\n"))
cat("a: ", vector.numerical.a, "\tb: ", vector.numerical.b,
"\na != b: ", vector.numerical.a != vector.numerical.b, "\n\n")
cat("a: ", vector.integer.a, "\tb: ", vector.integer.b,
"\na != b: ", vector.integer.a != vector.integer.b, "\n\n")
cat("a: ", vector.complex.a, "\tb: ", vector.complex.b,
"\na != b: ", vector.complex.a != vector.complex.b, "\n\n")
cat("a: ", vector.character.a, "\tb: ", vector.character.b,
"\na != b: ", vector.character.a != vector.character.b, "\n\n")

## Matrices.
cat(green("\n==============================================================\n"))
cat(green("Matrices:\n"))

cat(yellow("\nGreater:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na > b:\n")
print(matrix.numerical.a > matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na > b:\n")
print(matrix.integer.a > matrix.integer.b)
cat("\n\n")

cat(yellow("\nLess:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na < b:\n")
print(matrix.numerical.a < matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na < b:\n")
print(matrix.integer.a < matrix.integer.b)
cat("\n\n")

cat(yellow("\nEqual:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na == b:\n")
print(matrix.numerical.a == matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na == b:\n")
print(matrix.integer.a == matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na == b:\n")
print(matrix.complex.a == matrix.complex.b)
cat("\n\n")
cat("a:\n")
print(matrix.character.a)
cat("b:\n")
print(matrix.character.b)
cat("\na == b:\n")
print(matrix.character.a == matrix.character.b)
cat("\n\n")

cat(yellow("\nGreater or Equal:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na >= b:\n")
print(matrix.numerical.a >= matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na >= b:\n")
print(matrix.integer.a >= matrix.integer.b)
cat("\n\n")

cat(yellow("\nLess or Equal:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na <= b:\n")
print(matrix.numerical.a <= matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na <= b:\n")
print(matrix.integer.a <= matrix.integer.b)
cat("\n\n")

cat(yellow("\nDifferent:\n"))
cat("a:\n")
print(matrix.numerical.a)
cat("b:\n")
print(matrix.numerical.b)
cat("\na != b:\n")
print(matrix.numerical.a != matrix.numerical.b)
cat("\n\n")
cat("a:\n")
print(matrix.integer.a)
cat("b:\n")
print(matrix.integer.b)
cat("\na != b:\n")
print(matrix.integer.a != matrix.integer.b)
cat("\n\n")
cat("a:\n")
print(matrix.complex.a)
cat("b:\n")
print(matrix.complex.b)
cat("\na != b:\n")
print(matrix.complex.a != matrix.complex.b)
cat("\n\n")
cat("a:\n")
print(matrix.character.a)
cat("b:\n")
print(matrix.character.b)
cat("\na != b:\n")
print(matrix.character.a != matrix.character.b)
cat("\n\n")
