"""This file have the Haar wavelets analysis."""
import matplotlib.pyplot as plt
from scipy.signal import freqz
import numpy as np


class HaarWavelets:
    """The class for a Haar wavelets analysis."""

    def __init__(self, signal, n=1):
        """Contruct a Haar wavelets model."""
        self.signal = np.array(signal)
        self.N = len(self.signal)
        self.n = n

        self.d_n = []

        for i in range(self.n):
            self.a_n = self.get_a_n()
            self.d_n.append(self.get_d_n())
            self.signal = self.a_n
            self.N = len(self.signal)

        self.signal = np.array(signal)
        self.N = len(self.signal)
        self.H = np.append(self.a_n, self.d_n[-1])

        self.cumulative_E = self.get_cumulative_E()

    def get_a_n(self):
        """Compute the a_n vector."""
        a_n = []

        for n in range(0, self.N-1, 2):
            a_n.append(
                (self.signal[n] + self.signal[n+1])/np.sqrt(2)
            )

        return np.array(a_n)

    def get_d_n(self):
        """Compute the d_n vector."""
        d_n = []

        for n in range(0, self.N-1, 2):
            d_n.append(
                (self.signal[n] - self.signal[n+1])/np.sqrt(2)
            )

        return np.array(d_n)

    def get_cumulative_E(self):
        """Compute the d_n vector."""
        cumulative_E = {"signal": [], "transform": []}

        normalization = np.dot(self.signal, self.signal)

        for n in range(0, self.N):
            cumulative_E["signal"].append(
                np.dot(self.signal[:n+1], self.signal[:n+1])/normalization
            )
            cumulative_E["transform"].append(
                np.dot(self.H[:n+1], self.H[:n+1])/normalization
            )

        cumulative_E["signal"] = np.array(cumulative_E["signal"])
        cumulative_E["transform"] = np.array(cumulative_E["transform"])

        return cumulative_E

    def inverse(self, a_n=None, d_n=None):
        """Compute the inverse transform."""
        if a_n is None:
            a_n = self.a_n
        if d_n is None:
            d_n = self.d_n[-1]

        A = []
        D = []
        for n in range(len(a_n)):
            A.append(a_n[n]/np.sqrt(2))
            A.append(a_n[n]/np.sqrt(2))
            D.append(d_n[n]/np.sqrt(2))
            D.append(-d_n[n]/np.sqrt(2))

        A = np.array(A)
        D = np.array(D)

        return A + D

    def plot_signal(self, label):
        """Plot the signal in time."""
        plt.plot(self.signal)
        plt.title("Signal")
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_wavelet(self, label):
        """Plot the signal in time."""
        x = np.linspace(-0.5, 1.5, 200)
        y = []
        for x_i in x:
            if 0.0 <= x_i < 0.5:
                y.append(1)
            elif 0.5 <= x_i < 1:
                y.append(-1)
            else:
                y.append(0)
        plt.plot(x, y)
        plt.title("Mother Wavelet")
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_scaling(self, label):
        """Plot the signal in time."""
        x = np.linspace(-0.5, 1.5, 200)
        y = []
        for x_i in x:
            if 0.0 <= x_i < 1:
                y.append(1)
            else:
                y.append(0)
        plt.plot(x, y)
        plt.title("Scaling Function")
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_haar_transform(self, label):
        """Plot the signal in time."""
        plt.plot(self.signal, label="Signal")
        plt.plot(self.H, label="Transform")
        plt.title("Signal and Haar Transform")
        plt.legend()
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_compress(self, label):
        """Plot the signal in time."""
        plt.plot(range(self.N), self.signal, label="Signal")
        plt.plot(range(0, self.N, 2*self.n), self.a_n, label="Transform")
        plt.title("Compression of the signal")
        plt.legend()
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_denoise(self, label):
        """Plot the signal in time."""
        inverse = self.a_n
        for i in range(self.n):
            inverse = self.inverse(inverse, self.d_n[-i-1]/100)
        plt.plot(range(self.N), self.signal, label="Original Signal")
        plt.plot(
            np.linspace(0, self.N, len(inverse)),
            inverse, label="Denoised Transform"
        )
        plt.title("Denoising of the signal")
        plt.legend()
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_cumulative_E(self, label):
        """Plot the signal in time."""
        plt.plot(self.cumulative_E["signal"], label="Signal")
        plt.plot(self.cumulative_E["transform"], label="Tranform")
        plt.title("Cimulative Energy")
        plt.legend()
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()

    def plot_transfer(self, label):
        """Plot the transfer functions of the Haar wavelets."""
        w, h = freqz([1/np.sqrt(2), 1/np.sqrt(2)], 1)
        plt.plot(w, 20 * np.log10(abs(h)), label="Scaling signal")
        w, h = freqz([1/np.sqrt(2), -1/np.sqrt(2)], 1)
        plt.plot(w, 20 * np.log10(abs(h)), label="Wavelet signal")

        plt.title("Transfere function")
        plt.xlabel("Frequency [rad/sample]")
        plt.ylabel("Amplitude [dB]")
        plt.legend()
        plt.savefig(f"{label}.pdf")
        # plt.show()
        plt.clf()
