"""Study the database of PYR cells that will give the hetrogeneous model."""
from ca1_models.minimal_model.defaults import params
from ca1_models.minimal_model.neuron import neuron
import ca1_models.minimal_model.rk4.rk4 as rk4
from multiprocessing import Pool, cpu_count
import pandas as pd
import numpy as np


def rheo_test(test_neuron, t_total=500.0, h=0.1,
              I_input_vals=np.arange(-25.0, 30.0, 0.5)):
    """
    Set the rheobase for the neuron.

    Check if the neuron spike within t_total [ms] given an input current of
    I_input [pA].

    Arguments:
    ---------
    test_neuron: neuron
        The test neuron for the rheobase computation.
    t_total: float
        Total time of the test. [ms]
    h: float
        The time step. [ms]
    I_input_vals: array like
        The values of the input current. [pA]

    Return:
    ------
    The input value in which the neuron spiked first.
    If none make the neuron spike, "No value" is returned.

    """
    # Run over the input current values.
    for I_input in I_input_vals:
        # Reset the initial conditions of the neuron.
        test_neuron.V = params.PYR_neuron_params["V_i"]
        test_neuron.u = params.PYR_neuron_params["u_i"]

        # Set time to zero.
        t = 0

        # Evolve the system.
        while t < t_total:
            # rk4 step.
            rk4.rk4_Vu(test_neuron, t, h=h, I_other=I_input)
            # Update k.
            test_neuron.calculate_k()

            # Check the neuron fires, if so, return the current input current.
            if test_neuron.check_peak():
                return I_input

            # Time step.
            t += h

    # Return "No value" in case of none of the currents make the cell spike.
    return "No Value"


def pir_test(test_neuron, t_total=2000.0, h=0.1,
             I_input_vals=np.arange(0.0, -30, -0.5)):
    """
    Set the post-inhibitory bounce for the neuron.

    Check if the neuron spike after a hyperpolarization.

    Arguments:
    ---------
    test_neuron: neuron
        The test neuron for the rheobase computation.
    t_total: float
        Total time of the test. [ms]
    h: float
        The time step. [ms]
    I_input_vals: array like
        The values of the input current. [pA]

    Return:
    ------
    The input value in which the neuron spiked first.
    If none make the neuron spike, "No value" is returned.

    """
    # Run over the input current values.
    for I_input in I_input_vals:
        # Reset the initial conditions of the neuron.
        test_neuron.V = params.PYR_neuron_params["V_i"]
        test_neuron.u = params.PYR_neuron_params["u_i"]

        # Set time to zero.
        t = 0

        # Bool to check if the cell spikes before the step.
        pre_step_spike = False

        # Evolve the system.
        while t < t_total:
            # rk4 step. The input current is I_input the first 1s, and 0 after
            # it.
            if t < 1000:
                rk4.rk4_Vu(test_neuron, t, h=h, I_other=I_input)
            else:
                rk4.rk4_Vu(test_neuron, t, h=h)

            # Update k.
            test_neuron.calculate_k()

            # Check if the neuron fires.
            spike = test_neuron.check_peak()

            if spike and t <= 1000:
                pre_step_spike = True

            # If the neuron fires after the current step, and had not spike
            # before, then return the current current.
            if spike and not pre_step_spike and t > 1000:
                return I_input

            # Time step.
            t += h

    # Return "No value" in case of none of the currents make the cell spike
    # after the current step.
    return "No value"


def sfa_test(test_neuron, t_total=1000.0, h=0.1,
             I_input_vals=np.arange(0.0, 100.0, 2.0)):
    """
    Set the spike frequency adaptation for the neuron.

    Check the difference of the behavior of the initial and final frequency of
    the spikes across the input current.

    Arguments:
    ---------
    test_neuron: neuron
        The test neuron for the rheobase computation.
    t_total: float
        Total time of the test. [ms]
    h: float
        The time step. [ms]
    I_input_vals: array like
        The values of the input current. [pA]

    Return:
    ------
    The difference between the slope of the linear fit of the initial
    frequencies across the input current, and fit of the final frequencies
    across the input current.
    If none make the neuron spike, "No value" is returned.

    """
    # List for the initial frequency.
    freq_i = []
    # List for the final frequency.
    freq_f = []
    # List for the current with multiple spikes.
    I_spikes = []

    # Run over the input current values.
    for I_input in I_input_vals:
        # Reset the initial conditions of the neuron.
        test_neuron.V = params.PYR_neuron_params["V_i"]
        test_neuron.u = params.PYR_neuron_params["u_i"]

        # Set time to zero.
        t = 0

        # Set the spike time to zero.
        spike_time = 0
        # List for the difference of time between spikes.
        spikes_differences = []

        # Evolve the system.
        while t < t_total:
            # rk4 step.
            rk4.rk4_Vu(test_neuron, t, h=h, I_other=I_input)
            # Update k.
            test_neuron.calculate_k()

            # Check if the neuron spikes for the first time.
            if not spike_time and test_neuron.check_peak():
                spike_time = t
                continue

            # Check if the neuron spikes, to add the difference with the last
            # spike.
            if test_neuron.check_peak():
                spikes_differences.append(t - spike_time)
                spike_time = t

            # Time step.
            t += h

        # Add initial and final frequency, as well as the current current.
        if len(spikes_differences) >= 1:
            freq_i.append(1/spikes_differences[0])
            freq_f.append(1/spikes_differences[-1])
            I_spikes.append(I_input)

    # This test requires a linear fit, so, if the lists do not have more than
    # one entry, it returns "No value".
    if len(freq_f) <= 1:
        return "No value"

    # Get the slope of the linear fit of the initial frequency with the
    # current, as well as the final frequency with the current. Compute the
    # difference.
    sfa =\
        np.polyfit(I_spikes, freq_i, 1)[0] - np.polyfit(I_spikes, freq_f, 1)[0]

    return sfa


def create_database(a_vals=np.arange(0.0, 0.0024, 0.00024),
                    b_vals=np.arange(0.0, 6.0, 0.6),
                    d_vals=np.arange(0.0, 20.0, 2.0),
                    k_low_vals=np.arange(0.0, 0.20, 0.02)):
    """
    Create the database of the PYR neurons.

    Create a PYR neuron with value of the parameters passed as arguments,
    (a_vals, b_vals, d_vals, k_low_vals), and set the rheobase (Rheo), the
    spike frequency adaptation (SFA) and the post-inhibitory rebound (PIR).

    Arguments:
    ---------
    a_vals: array like
        An array with the values that the parameter a (the recovery time
        constant of the adaptation current) will take. [ms^-1]
    b_vals: array like
        An array with the values that the parameter b (the sensitivity of the
        adaptation current to subthresholds fluctuations) will take. [nS]
    d_vals: array like
        An array with the values that the parameter d (the total amount of
        backwards and inwards currents activated during the spike, that affects
        the after-spike behavior) will take. [pA]
    k_low_vals: array like
        An array with the values that the parameter k_low (low scaling factor)
        will take. [nS/mV]

    Return:
    ------
    The dataframe with the values of a, b, d, k_low, Rheo, SFA and PIR.

    """
    print("Create all database.")

    # Create the dictionary for the database.
    database = {"a": [], "b": [], "d": [], "k_low": [],
                "Rheo": [], "SFA": [], "PIR": []}

    # Create the pool of processes.
    pool = Pool(processes=int(cpu_count()))

    # Create all the neurons for the test.
    all_k_neurons = [
        neuron.PYR_neuron(params.PYR_neuron_params["V_i"],
                          params.PYR_neuron_params["u_i"],
                          a=a, b=b, d=d, k_low=k_low)
        for a in a_vals
        for b in b_vals
        for d in d_vals
        for k_low in k_low_vals
    ]

    # Run the tests in parallel.
    process = [
        [
            test_neuron.a,
            test_neuron.b,
            test_neuron.d,
            test_neuron.k_low,
            pool.apply_async(rheo_test, [test_neuron]),
            pool.apply_async(pir_test, [test_neuron]),
            pool.apply_async(sfa_test, [test_neuron])
        ]
        for test_neuron in all_k_neurons
    ]

    # Get the tests results and store in the database variable.
    for index in range(len(process)):

        database["a"].append(process[index][0])
        database["b"].append(process[index][1])
        database["d"].append(process[index][2])
        database["k_low"].append(process[index][3])

        database["Rheo"].\
            append(process[index][4].get(timeout=1000))
        database["PIR"].\
            append(process[index][5].get(timeout=1000))
        database["SFA"].\
            append(process[index][6].get(timeout=1000))

        print(f"[{(index + 1)/len(process)*100:.2f}%]", end="\r")
    print()
    print("All database created.")

    # Return the database.
    return pd.DataFrame(database)


def create_Rheo_database(a_vals=np.arange(0.0, 0.0024, 0.00024),
                         b_vals=np.arange(0.0, 6.0, 0.6),
                         d_vals=np.arange(0.0, 20.0, 2.0),
                         k_low_vals=np.arange(0.0, 0.20, 0.02)):
    """
    Create the database for the Rheo parameter of the PYR neurons.

    Create a PYR neuron with value of the parameters passed as arguments,
    (a_vals, b_vals, d_vals, k_low_vals), and set the rheobase (Rheo).

    Arguments:
    ---------
    a_vals: array like
        An array with the values that the parameter a (the recovery time
        constant of the adaptation current) will take. [ms^-1]
    b_vals: array like
        An array with the values that the parameter b (the sensitivity of the
        adaptation current to subthresholds fluctuations) will take. [nS]
    d_vals: array like
        An array with the values that the parameter d (the total amount of
        backwards and inwards currents activated during the spike, that affects
        the after-spike behavior) will take. [pA]
    k_low_vals: array like
        An array with the values that the parameter k_low (low scaling factor)
        will take. [nS/mV]

    Return:
    ------
    The dataframe with the values of a, b, d, k_low and Rheo.

    """
    print("Create Rheo database.")

    # Create the dictionary for the database.
    database = {"a": [], "b": [], "d": [], "k_low": [],
                "Rheo": []}

    # Create the pool of processes.
    pool = Pool(processes=int(cpu_count()))

    # Create all the neurons for the test.
    all_k_neurons = [
        neuron.PYR_neuron(params.PYR_neuron_params["V_i"],
                          params.PYR_neuron_params["u_i"],
                          a=a, b=b, d=d, k_low=k_low)
        for a in a_vals
        for b in b_vals
        for d in d_vals
        for k_low in k_low_vals
    ]

    # Run the tests in parallel.
    process = [
        [
            test_neuron.a,
            test_neuron.b,
            test_neuron.d,
            test_neuron.k_low,
            pool.apply_async(rheo_test, [test_neuron])
        ]
        for test_neuron in all_k_neurons
    ]

    # Get the tests results and store in the database variable.
    for index in range(len(process)):

        database["a"].append(process[index][0])
        database["b"].append(process[index][1])
        database["d"].append(process[index][2])
        database["k_low"].append(process[index][3])

        database["Rheo"].\
            append(process[index][4].get(timeout=1000))

        print(f"[{(index + 1)/len(process)*100:.2f}%]", end="\r")
    print()
    print("Rheo database created.")

    # Return the database.
    return pd.DataFrame(database)


def create_PIR_database(a_vals=np.arange(0.0, 0.0024, 0.00024),
                        b_vals=np.arange(0.0, 6.0, 0.6),
                        d_vals=np.arange(0.0, 20.0, 2.0),
                        k_low_vals=np.arange(0.0, 0.20, 0.02)):
    """
    Create the database for the PIR parameter of the PYR neurons.

    Create a PYR neuron with value of the parameters passed as arguments,
    (a_vals, b_vals, d_vals, k_low_vals), and set the post-inhibitory rebound
    (PIR).

    Arguments:
    ---------
    a_vals: array like
        An array with the values that the parameter a (the recovery time
        constant of the adaptation current) will take. [ms^-1]
    b_vals: array like
        An array with the values that the parameter b (the sensitivity of the
        adaptation current to subthresholds fluctuations) will take. [nS]
    d_vals: array like
        An array with the values that the parameter d (the total amount of
        backwards and inwards currents activated during the spike, that affects
        the after-spike behavior) will take. [pA]
    k_low_vals: array like
        An array with the values that the parameter k_low (low scaling factor)
        will take. [nS/mV]

    Return:
    ------
    The dataframe with the values of a, b, d, k_low and PIR.

    """
    print("Create PIR database.")

    # Create the dictionary for the database.
    database = {"a": [], "b": [], "d": [], "k_low": [],
                "PIR": []}

    # Create the pool of processes.
    pool = Pool(processes=int(cpu_count()))

    # Create all the neurons for the test.
    all_k_neurons = [
        neuron.PYR_neuron(params.PYR_neuron_params["V_i"],
                          params.PYR_neuron_params["u_i"],
                          a=a, b=b, d=d, k_low=k_low)
        for a in a_vals
        for b in b_vals
        for d in d_vals
        for k_low in k_low_vals
    ]

    # Run the tests in parallel.
    process = [
        [
            test_neuron.a,
            test_neuron.b,
            test_neuron.d,
            test_neuron.k_low,
            pool.apply_async(pir_test, [test_neuron])
        ]
        for test_neuron in all_k_neurons
    ]

    # Get the tests results and store in the database variable.
    for index in range(len(process)):

        database["a"].append(process[index][0])
        database["b"].append(process[index][1])
        database["d"].append(process[index][2])
        database["k_low"].append(process[index][3])

        database["PIR"].\
            append(process[index][4].get(timeout=1000))

        print(f"[{(index + 1)/len(process)*100:.2f}%]", end="\r")
    print()
    print("PIR database created.")

    # Return the database.
    return pd.DataFrame(database)


def create_SFA_database(a_vals=np.arange(0.0, 0.0024, 0.00024),
                        b_vals=np.arange(0.0, 6.0, 0.6),
                        d_vals=np.arange(0.0, 20.0, 2.0),
                        k_low_vals=np.arange(0.0, 0.20, 0.02)):
    """
    Create the database for the SFA parameter of the PYR neurons.

    Create a PYR neuron with value of the parameters passed as arguments,
    (a_vals, b_vals, d_vals, k_low_vals), and set the spike frequency
    adaptation (SFA).

    Arguments:
    ---------
    a_vals: array like
        An array with the values that the parameter a (the recovery time
        constant of the adaptation current) will take. [ms^-1]
    b_vals: array like
        An array with the values that the parameter b (the sensitivity of the
        adaptation current to subthresholds fluctuations) will take. [nS]
    d_vals: array like
        An array with the values that the parameter d (the total amount of
        backwards and inwards currents activated during the spike, that affects
        the after-spike behavior) will take. [pA]
    k_low_vals: array like
        An array with the values that the parameter k_low (low scaling factor)
        will take. [nS/mV]

    Return:
    ------
    The dataframe with the values of a, b, d, k_low and SFA.

    """
    print("Create SFA database.")

    # Create the dictionary for the database.
    database = {"a": [], "b": [], "d": [], "k_low": [],
                "SFA": []}

    # Create the pool of processes.
    pool = Pool(processes=int(cpu_count()))

    # Create all the neurons for the test.
    all_k_neurons = [
        neuron.PYR_neuron(params.PYR_neuron_params["V_i"],
                          params.PYR_neuron_params["u_i"],
                          a=a, b=b, d=d, k_low=k_low)
        for a in a_vals
        for b in b_vals
        for d in d_vals
        for k_low in k_low_vals
    ]

    # Run the tests in parallel.
    process = [
        [
            test_neuron.a,
            test_neuron.b,
            test_neuron.d,
            test_neuron.k_low,
            pool.apply_async(sfa_test, [test_neuron])
        ]
        for test_neuron in all_k_neurons
    ]

    # Get the tests results and store in the database variable.
    for index in range(len(process)):

        database["a"].append(process[index][0])
        database["b"].append(process[index][1])
        database["d"].append(process[index][2])
        database["k_low"].append(process[index][3])

        database["SFA"].\
            append(process[index][4].get(timeout=1000))

        print(f"[{(index + 1)/len(process)*100:.2f}%]", end="\r")
    print()
    print("SFA database created.")

    # Return the database.
    return pd.DataFrame(database)
