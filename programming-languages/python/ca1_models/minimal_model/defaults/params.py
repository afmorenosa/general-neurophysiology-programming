"""All the default parameters as dictionaries."""
# Pyramidal (PYR) neurons parameters.
PYR_neuron_params = {
    "V_i": -61.8,
    "u_i": 0,
    "C_m": 115.0,
    "k_low": 0.1,
    "k_high": 3.3,
    "v_r": -61.8,
    "v_t": -57.0,
    "v_peak": 22.6,
    "a": 0.0012,
    "b": 3.0,
    "c": -65.8,
    "d": 10.0
}

# Parvalbumin-expressing (PV+) neurons parameters.
PV_plus_neuron_params = {
    "V_i": -60.6,
    "u_i": 0,
    "C_m": 90.0,
    "k_low": 1.7,
    "k_high": 14.0,
    "v_r": -60.6,
    "v_t": -43.1,
    "v_peak": -2.5,
    "a": 0.1,
    "b": -0.1,
    "c": -67.0,
    "d": 0.1,
}

# Synapse from Pyramidal (PYR) neuron to Pyramidal (PYR) neuron parameters.
PYR_to_PYR_synapse_params = {
    "tau_R": 0.5,
    "tau_D": 3.0,
    "g": 0.094,
    "E_rev": -15
}

# Synapse from Pyramidal (PYR) neuron to Parvalbumin-expressing (PV+) neuron
# parameters.
PYR_to_PV_plus_synapse_params = {
    "tau_R": 0.37,
    "tau_D": 2.1,
    "g": 3.0,
    "E_rev": -15
}

# Synapse from Parvalbumin-expressing (PV+) neuron to Pyramidal (PYR) neuron
# parameters.
PV_plus_to_PYR_synapse_params = {
    "tau_R": 0.3,
    "tau_D": 3.5,
    "g": 8.7,
    "E_rev": -85
}

# Synapse from Parvalbumin-expressing (PV+) neuron to Parvalbumin-expressing
# (PV+) neuron parameters.
PV_plus_to_PV_plus_synapse_params = {
    "tau_R": 0.27,
    "tau_D": 1.7,
    "g": 3.0,
    "E_rev": -85
}
