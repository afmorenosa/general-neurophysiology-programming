"""Runge-Kutta 4 method."""
from ca1_models.minimal_model.neuron import diff_eq
import numpy as np


def rk4_Vu(in_neuron, t, h=0.1, I_syn=0, I_other=0):
    """
    Runge-Kutta method applied to the V u differential equations.

    This function applies the Runge-Kutta method for the following coupled
    equations:

          [k (V - v_r) (V - v_t) - u + I_other - I_syn]
    V' = -----------------------------------------------
                               C_m

    u' = a [b (V - v_r) - u]

    where C_m, k, v_r, v_t, I_other, I_syn, a and b are paramethers given by
    the neuron. Reffer to neuron class documentation for more information about
    the meaning, units and characteristics of each parameter and variable.

    Arguments:
    ---------
    in_neuron: neuron
        The input neuron.
    t: float
        The time. [ms]
    h: float
        The time step. [ms]
    I_syn: float
        Synaptic input. [pA]
    I_other: float
        Synaptic from the "other" source (noise). [pA]

    """
    # k_1.
    kV_1 = in_neuron.V_diff(t, 0, 0, I_syn, I_other) * h
    ku_1 = in_neuron.u_diff(t, 0, 0) * h

    # k_2.
    kV_2 = in_neuron.V_diff(t + h/2, kV_1/2, ku_1/2, I_syn, I_other) * h
    ku_2 = in_neuron.u_diff(t + h/2, kV_1/2, ku_1/2) * h

    # k_3.
    kV_3 = in_neuron.V_diff(t + h/2, kV_2/2, ku_2/2, I_syn, I_other) * h
    ku_3 = in_neuron.u_diff(t + h/2, kV_2/2, ku_2/2) * h

    # k_4.
    kV_4 = in_neuron.V_diff(t + h, kV_3, ku_3, I_syn, I_other) * h
    ku_4 = in_neuron.u_diff(t + h, kV_3, ku_3) * h

    # Update the values for V and u.
    in_neuron.V += (kV_1 + 2*kV_2 + 2*kV_3 + kV_4)/6
    in_neuron.u += (ku_1 + 2*ku_2 + 2*ku_3 + ku_4)/6


def rk4_s(s, t, h, alpha, T, beta):
    """
    Runge-Kutta method applied to the s differential equation.

    This function applies the Runge-Kutta method for the following differential
    equation:

    s' = alpha * [T] * (1 - s) - beta * s

    where alpha, T and beta are parameters.

    Arguments:
    ---------
    s: float
        The actual state of the fraction of the open synaptic channels.
        [dimensionless]
    t: float
        The time. [ms]
    h: float
        The time step. [ms]
    alpha: float
        Parameter related to the inverse of the raise and decay time constans.
        [mM^-1 ms^-1]
    T: float
        The concentration of the transmitter released by a presynaptic spike.
        [mM]
    beta: float
        Parameter related to the inverse of the raise and decay time constans.
        [ms^-1]

    """
    # k_1.
    k_1 = diff_eq.s_diff(s, t, alpha, T, beta) * h

    # k_2.
    k_2 = diff_eq.s_diff(s + k_1/2, t + h/2, alpha, T, beta) * h

    # k_3.
    k_3 = diff_eq.s_diff(s + k_2/2, t + h/2, alpha, T, beta) * h

    # k_4.
    k_4 = diff_eq.s_diff(s + k_3, t + h, alpha, T, beta) * h

    # Update the values for s.
    return s + (k_1 + 2*k_2 + 2*k_3 + k_4)/6


def rk4_g_e(g_e, t, h, g_e_mean=0, sigma_e=0.6, tau_e=2.73):
    """
    Runge-Kutta method applied to the g_e differential equation.

    This function applies the Runge-Kutta method for the following differential
    equation:

                                       _________________
               (g_e - g_e_mean)       |  2 * sigma_e^2
    g_e' = -  ------------------  +   | ---------------   * Chi_e(t)
                     tau_e           ╲|      tau_e


    where g_e_mean, tau_e and sigma_e are parameters, and Chi_e(t) is a
    independient Gaussian with unit standard deviation and zero mean.

    Arguments:
    ---------
    g_e: float
        The actual stochastic process value.
    t: float
        The time. [ms]
    h: float
        The time step. [ms]
    g_e_mean: float
        Average conductance. [mS]
    sigma_e: float
        Noise standard deviation. [nS]
    tau_e: float
        Time constant for excitatory synapses. [ms]

    """
    # Gaussian random numbers.
    Chi_e = np.random.normal()

    # k_1.
    k_1 = diff_eq.g_e_diff(g_e, t, Chi_e, g_e_mean, sigma_e, tau_e) * h

    # k_2.
    k_2 = diff_eq.g_e_diff(g_e + k_1/2, t + h/2, Chi_e,
                           g_e_mean, sigma_e, tau_e) * h

    # k_3.
    k_3 = diff_eq.g_e_diff(g_e + k_2/2, t + h/2, Chi_e,
                           g_e_mean, sigma_e, tau_e) * h

    # k_4.
    k_4 = diff_eq.g_e_diff(g_e + k_3, t + h, Chi_e,
                           g_e_mean, sigma_e, tau_e) * h

    # Update the values for g_e.
    return g_e + (k_1 + 2*k_2 + 2*k_3 + k_4)/6
