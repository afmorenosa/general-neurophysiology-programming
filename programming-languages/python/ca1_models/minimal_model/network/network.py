"""This code give the classes to create a neuronal network."""
from ca1_models.minimal_model.neuron import neuron, synapse
from ca1_models.minimal_model.defaults import params
import ca1_models.minimal_model.rk4.rk4 as rk4
import numpy as np


class neuronal_network:
    """
    Class for a basic neuronal network.

    This class creates a homogeneous neuronal network, with a specific number
    of neurons. this class can connect its own neurons according a probability
    connection as well as it can connect with neurons in other neuronal
    network.

    This is a base class with empty neurons.

    """

    def __init__(self, name, n_neurons, t=0, h=0.1, s_i=0):
        """
        Neuronal network constructor.

        This constructor creates a neuronal network with n_neurons number of
        neurons. This network has empty neurons.

        Arguments:
        ---------
        name: str
            The name of the neuronal network, it should be unique be since the
            connection between two networks is given by a value of a
            dictionary which key is the network name.
        n_neurons: int
            The number of neurons in the network.
        t: float
            The initial time. [ms]
        h: float
            The time step. [ms]
        s_i: float
            The initial value for the fraction of the synaptic channel.
            [dimensionless]

        """
        # Set the neuronal network name.
        self.name = name

        # Set the neuronal network time.
        self.t = t

        # Set the neuronal network time step.
        self.h = h

        # Set the number of neurons.
        self.n_neurons = n_neurons

        # Create the connections dictionary.
        self.connections = {}

        # Create the presynaptic network dictionary.
        self.presynaptic_networks = {}

        # Create the matrix of the concentration of the transmitter released by
        # a presynaptic spike.
        self.T_spike = np.zeros(self.n_neurons)

        # Create the matrix that have if a neuron had a peak or not.
        self.peak_time = np.zeros(self.n_neurons)

        # Initialize the neuron array.
        self.neurons = []

        # Initialize initial value for the fraction of the open synaptic
        # channel array.
        self.s_i = s_i

        # Initialize the fraction of the synaptic channel dictionary.
        self.s = {}

        # Create the neurons.
        for i in range(n_neurons):
            self.neurons.append(neuron.neuron(0, 0, 0, 0, 0, 0,
                                              0, 0, 0, 0, 0, 0))

        self.neurons = np.array(self.neurons)

    def connect_internal_neurons(self, p, parameters):
        """
        Connect the neurons in the network.

        Connects the neurons inside the neuronal network according to a
        probability of connection.

        Arguments:
        ---------
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        # Check probability value.
        if 0 < p > 1:
            raise ValueError("Probability is not between 0 and 1.")

        # Create connection matrix
        all_connections = []

        # Create the fraction of the synaptic channel matrix
        self.s["internal"] = np.zeros((self.n_neurons,
                                       self.n_neurons))

        # Run over presynaptic neurons.
        for pre in range(self.n_neurons):
            connection = []
            # Run over postynaptic neurons.
            for post in range(self.n_neurons):
                if pre == post:
                    connection.append(None)
                    self.s["internal"][pre][post] = None
                elif np.random.choice([True, False], p=[p, 1-p]):
                    connection.append(
                        synapse.synapse(parameters["tau_R"],
                                        parameters["tau_D"],
                                        parameters["g"],
                                        parameters["E_rev"])
                    )
                    self.s["internal"][pre][post] = self.s_i
                else:
                    connection.append(None)
                    self.s["internal"][pre][post] = None

            all_connections.append(connection)

        self.connections["internal"] = np.array(all_connections)

    def connect_network(self, postsynaptic_network, p, parameters):
        """
        Connect the network with the postsynaptic network.

        Connects the neurons inside the neuronal network with the neurons in
        the postynaptic network according to a probability of connection.

        Arguments:
        ---------
        postsynaptic_network: network
            The network wich will have the postynaptic neurons.
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        # Check probability value.
        if 0 < p > 1:
            raise ValueError("Probability is not between 0 and 1.")

        # Set the presynaptic network
        postsynaptic_network.add_presynaptic_network_synapse(self)

        # Create connection matrix
        all_connections = []

        # Create the fraction of the synaptic channel matrix
        self.s[postsynaptic_network.name] =\
            np.zeros((self.n_neurons,
                      postsynaptic_network.n_neurons))

        # Run over presynaptic neurons.
        for pre in range(self.n_neurons):
            connection = []
            # Run over postynaptic neurons.
            for post in range(postsynaptic_network.n_neurons):
                if np.random.choice([True, False], p=[p, 1-p]):
                    connection.append(
                        synapse.synapse(parameters["tau_R"],
                                        parameters["tau_D"],
                                        parameters["g"],
                                        parameters["E_rev"])
                    )
                    self.s[postsynaptic_network.name][pre][post] = self.s_i
                else:
                    connection.append(None)
                    self.s[postsynaptic_network.name][pre][post] = None

            all_connections.append(connection)

        self.connections[postsynaptic_network.name] = np.array(all_connections)

    def add_presynaptic_network_synapse(self, presynaptic_network):
        """
        Add the presynaptic network.

        Add the presynaptic network to the presynaptic networks dictionary.

        Arguments:
        ---------
        presynaptic_network: network
            The network wich has the preynaptic connection matrix.

        """
        # Add the presynaptic network.
        self.presynaptic_networks[presynaptic_network.name] =\
            presynaptic_network

    def time_step(self, I_other=0.0):
        """
        Add the presynaptic network.

        Add the presynaptic network to the presynaptic networks dictionary.

        Arguments:
        ---------
        I_other: float, array like
            The noise synaptic input of the neurons, it can be the same value
            for all the neurons (float), or a value for each neuron (array
            like). In case of an array its length must be n_neurons. [pA]

        """
        # Run over the network neurons.
        for net_neuron in range(self.n_neurons):

            # Get the input currents of the synapses.
            # Set input synapse current to zero.
            I_syn = 0

            # Run over all the presynaptic networks.
            for net in self.presynaptic_networks:
                # Get matrcies
                C_input = self.presynaptic_networks[net].connections[self.name]
                s_input = self.presynaptic_networks[net].s[self.name]

                # run over the neurons that send current to the network neuron.
                for pre in range(self.presynaptic_networks[net].n_neurons):
                    # Check the connection.
                    if not C_input[pre][net_neuron]:
                        continue

                    # Add the current.
                    I_syn += C_input[pre][net_neuron].\
                        get_the_synaptic_input(s_input[pre][net_neuron],
                                               self.neurons[net_neuron].V)

            # Get the input current in the internal connections
            C_input = self.connections["internal"]
            s_input = self.s["internal"]

            # run over the neurons that send current to the network neuron.
            for pre in range(self.n_neurons):
                # Check the connection.
                if not C_input[pre][net_neuron]:
                    continue

                # Add the current.
                I_syn += C_input[pre][net_neuron].\
                    get_the_synaptic_input(s_input[pre][net_neuron],
                                           self.neurons[net_neuron].V)

            # Update the V and u values.
            if isinstance(I_other, (int, float)):
                rk4.rk4_Vu(self.neurons[net_neuron], self.t, self.h,
                           I_syn=I_syn, I_other=I_other)
            elif isinstance(I_other, (list, tuple, np.ndarray)):
                rk4.rk4_Vu(self.neurons[net_neuron], self.t, self.h,
                           I_syn=I_syn, I_other=I_other[net_neuron])

            # Calculate k
            self.neurons[net_neuron].calculate_k()
            # Check peak.
            if self.peak_time[net_neuron]:
                if (self.t - self.peak_time[net_neuron] >= 1):
                    self.peak_time[net_neuron] = 0
                    self.T_spike[net_neuron] = 0

            else:
                if self.neurons[net_neuron].check_peak():
                    self.peak_time[net_neuron] = self.t
                    self.T_spike[net_neuron] = 1

            # Update the Inputs that this network sends.
            for net in self.connections:
                # Get matrcies
                C = self.connections[net]
                s = self.s[net]

                # run over the neurons that receive current from the network
                # neuron.
                for post in range(len(C[net_neuron])):
                    # Check the connection.
                    if not C[net_neuron][post]:
                        continue

                    self.s[net][net_neuron][post] =\
                        rk4.rk4_s(s[net_neuron][post], self.t, self.h,
                                  1/C[net_neuron][post].tau_R,
                                  self.T_spike[net_neuron],
                                  1/C[net_neuron][post].tau_D)

        # Update time
        self.t += self.h


class homogeneous_neuronal_network(neuronal_network):
    """
    Class for a homogeneous neuronal network of a type of neuron.

    This class creates a homogeneous neuronal network of a given type of
    neuron, with a specific number of neurons. this class can connect its own
    neurons according a probability connection as well as it can connect with
    neurons in other neuronal network.

    """

    def __init__(self, name, n_neurons, neuron_contructor, parameters,
                 t=0, h=0.1, s_i=0):
        """
        Homogeneous neuronal network constructor.

        This constructor creates a neuronal network with n_neurons number of
        neurons. Each neuron is constructed with the neuron_constructor, so it
        gives the type of the neuron.

        Arguments:
        ---------
        name: str
            The name of the neuronal network, it should be unique be since the
            connection between two networks is given by a value of a
            dictionary which key is the network name.
        neuron_contructor: neuron class constructor
            The constructor of the neuron for the neuronal network.
        parameters: dictionary
            A dictionary with all the parameters to create the neuron. It needs
            to have the following keys:
                * V_i
                * u_i
                * C_m
                * k_low
                * k_high
                * v_r
                * v_t
                * v_peak
                * a
                * b
                * c
                * d
            All the values must be floats.
        n_neurons: int
            The number of neurons in the network.
        t: float
            The initial time. [ms]
        h: float
            The time step. [ms]
        s_i: float
            The initial value for the fraction of the synaptic channel.
            [dimensionless]

        """
        # Use neuronal_network contructor.
        super().__init__(name, n_neurons, t, h, s_i)

        # Create the neurons.
        for i in range(n_neurons):
            self.neurons[i] = neuron_contructor(V_i=parameters["V_i"],
                                                u_i=parameters["u_i"],
                                                C_m=parameters["C_m"],
                                                k_low=parameters["k_low"],
                                                k_high=parameters["k_high"],
                                                v_r=parameters["v_r"],
                                                v_t=parameters["v_t"],
                                                v_peak=parameters["v_peak"],
                                                a=parameters["a"],
                                                b=parameters["b"],
                                                c=parameters["c"],
                                                d=parameters["d"]
                                                )


class homogeneous_PYR_neuronal_network(homogeneous_neuronal_network):
    """
    Class for a homogeneous neuronal network of PYR neuron.

    This class creates a homogeneous neuronal network of pyramidal (PYR)
    neurons, with a specific number of neurons. this class can connect its own
    neurons according a probability connection as well as it can connect with
    neurons in other neuronal network.

    """

    def __init__(self, name, n_neurons, neuron_contructor=neuron.PYR_neuron,
                 parameters=params.PYR_neuron_params, t=0, h=0.1, s_i=0):
        """
        Homogeneous PYR neuronal network constructor.

        This constructor creates a neuronal network with n_neurons number of
        neurons. Each neuron is constructed with the neuron.PYR_neuron
        constructor, so all the neurons are pyramidal (PYR).

        Arguments:
        ---------
        name: str
            The name of the neuronal network, it should be unique be since the
            connection between two networks is given by a value of a
            dictionary which key is the network name.
        neuron_contructor: neuron class constructor
            The constructor of the neuron for the neuronal network.
        parameters: dictionary
            A dictionary with all the parameters to create the neuron. It needs
            to have the following keys:
                * V_i
                * u_i
                * C_m
                * k_low
                * k_high
                * v_r
                * v_t
                * v_peak
                * a
                * b
                * c
                * d
            All the values must be floats.
        n_neurons: int
            The number of neurons in the network.
        t: float
            The initial time. [ms]
        h: float
            The time step. [ms]
        s_i: float
            The initial value for the fraction of the synaptic channel.
            [dimensionless]

        """
        # Use neuronal_network contructor.
        super().__init__(name, n_neurons, neuron_contructor,
                         parameters, t, h, s_i)

    def connect_internal_neurons(self, p,
                                 parameters=params.PYR_to_PYR_synapse_params):
        """
        Connect the neurons in the network.

        Connects the neurons inside the neuronal network according to a
        probability of connection. Since this network is made with PYR neurons,
        the parameters of the sypanse are give the PYR to PYR connection.

        Arguments:
        ---------
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        super().connect_internal_neurons(p, parameters)

    def connect_PYR_network(self, postsynaptic_network, p,
                            parameters=params.PYR_to_PYR_synapse_params):
        """
        Connect the network with a postsynaptic PYR network.

        Connects the neurons inside the neuronal network with the neurons in a
        postynaptic PYR network according to a probability of connection, using
        the PYR to PYR connection parameters.

        Arguments:
        ---------
        postsynaptic_network: network
            The network wich will have the postynaptic neurons.
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        self.connect_network(postsynaptic_network, p, parameters)

    def connect_PV_plus_network(
     self, postsynaptic_network, p,
     parameters=params.PYR_to_PV_plus_synapse_params):
        """
        Connect the network with a postsynaptic PV+ network.

        Connects the neurons inside the neuronal network with the neurons in a
        postynaptic PV+ network according to a probability of connection, using
        the PYR to PV+ connection parameters.

        Arguments:
        ---------
        postsynaptic_network: network
            The network wich will have the postynaptic neurons.
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        self.connect_network(postsynaptic_network, p, parameters)


class homogeneous_PV_plus_neuronal_network(homogeneous_neuronal_network):
    """
    Class for a homogeneous neuronal network of PV+ neuron.

    This class creates a homogeneous neuronal network of parvalbumin-expressing
    (PV+) neurons, with a specific number of neurons. this class can connect
    its own neurons according a probability connection as well as it can
    connect with neurons in other neuronal network.

    """

    def __init__(self, name, n_neurons,
                 neuron_contructor=neuron.PV_plus_neuron,
                 parameters=params.PV_plus_neuron_params, t=0, h=0.1, s_i=0):
        """
        Homogeneous pyramidal (PYR) neuronal network constructor.

        This constructor creates a neuronal network with n_neurons number of
        neurons. Each neuron is constructed with the neuron.PV_plus_neuron
        constructor, so all the neurons are parvalbumin-expressing (PV+).

        Arguments:
        ---------
        name: str
            The name of the neuronal network, it should be unique be since the
            connection between two networks is given by a value of a
            dictionary which key is the network name.
        neuron_contructor: neuron class constructor
            The constructor of the neuron for the neuronal network.
        parameters: dictionary
            A dictionary with all the parameters to create the neuron. It needs
            to have the following keys:
                * V_i
                * u_i
                * C_m
                * k_low
                * k_high
                * v_r
                * v_t
                * v_peak
                * a
                * b
                * c
                * d
            All the values must be floats.
        n_neurons: int
            The number of neurons in the network.
        t: float
            The initial time. [ms]
        h: float
            The time step. [ms]
        s_i: float
            The initial value for the fraction of the synaptic channel.
            [dimensionless]

        """
        # Use neuronal_network contructor.
        super().__init__(name, n_neurons, neuron_contructor,
                         parameters, t, h, s_i)

    def connect_internal_neurons(
     self, p,
     parameters=params.PV_plus_to_PV_plus_synapse_params):
        """
        Connect the neurons in the network.

        Connects the neurons inside the neuronal network according to a
        probability of connection. Since this network is made with PV+ neurons,
        the parameters of the sypanse are give the PV+ to PV+ connection.

        Arguments:
        ---------
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        super().connect_internal_neurons(p, parameters)

    def connect_PV_plus_network(
     self, postsynaptic_network, p,
     parameters=params.PV_plus_to_PV_plus_synapse_params):
        """
        Connect the network with a postsynaptic PV+ network.

        Connects the neurons inside the neuronal network with the neurons in a
        postynaptic PV+ network according to a probability of connection, using
        the PV+ to PV+ connection parameters.

        Arguments:
        ---------
        postsynaptic_network: network
            The network wich will have the postynaptic neurons.
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        self.connect_network(postsynaptic_network, p, parameters)

    def connect_PYR_network(
     self, postsynaptic_network, p,
     parameters=params.PV_plus_to_PYR_synapse_params):
        """
        Connect the network with a postsynaptic PYR network.

        Connects the neurons inside the neuronal network with the neurons in a
        postynaptic PYR network according to a probability of connection, using
        the PV+ to PYR connection parameters.

        Arguments:
        ---------
        postsynaptic_network: network
            The network wich will have the postynaptic neurons.
        p: float
            A number between 0 and 1 that inidcates the probability of
            connections.
        parameters: dictionary
            A dictionary with all the parameters of the synapse. It needs to
            have the following keys:
                * tau_R
                * tau_D
                * g
                * E_rev
            All the values must be floats.

        """
        self.connect_network(postsynaptic_network, p, parameters)
