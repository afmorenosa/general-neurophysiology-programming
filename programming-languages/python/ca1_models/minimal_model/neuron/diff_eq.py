"""Differential equations relevant in CA1 region neurons."""
import numpy as np


def s_diff(s, t, alpha, T, beta):
    """
    Differential equation for s.

    Differential equation describing the fraction of the open synaptic
    channels:

    s' = alpha * [T] * (1 - s) - beta * s

    Arguments:
    ---------
    s: float
        The fraction of the open synaptic channels. [dimensionless]
    t: float
        The time. [ms]
    alpha: float
        Parameter related to the inverse of the raise and decay time constans.
        [mM^-1 ms^-1]
    T: float
        The concentration of the transmitter released by a presynaptic spike.
        [mM]
    beta: float
        Parameter related to the inverse of the raise and decay time constans.
        [ms^-1]

    Return:
    ------
    The value for s'.

    """
    return alpha * T * (1 - s) - beta * s


def g_e_diff(g_e, t, Chi_e, g_e_mean=0, sigma_e=0.6, tau_e=2.73):
    """
    Differential equation for g_e.

    Differential equation describing a stochastic process:

                                       _________________
               (g_e - g_e_mean)       |  2 * sigma_e^2
    g_e' = -  ------------------  +   | ---------------   * Chi_e(t)
                     tau_e           ╲|      tau_e

    where g_e_mean, tau_e and sigma_e are parameters, and Chi_e(t) is a
    independient Gaussian with unit standard deviation and zero mean.

    Arguments:
    ---------
    g_e: float
        The stochastic process value.
    t: float
        The time. [ms]
    Chi_e: float
        Random value from a independient Gaussian with unit standard deviation
        and zero mean. [ms^-1]
    g_e_mean: float
        Average conductance. [mS]
    sigma_e: float
        Noise standard deviation. [nS]
    tau_e: float
        Time constant for excitatory synapses. [ms]

    Return:
    ------
    The value for g_e'.

    """
    # Expresion for fractions.
    frac_1 = (g_e - g_e_mean) / tau_e
    frac_2 = (2 * sigma_e * sigma_e) / tau_e

    return -frac_1 + np.sqrt(frac_2) * Chi_e
